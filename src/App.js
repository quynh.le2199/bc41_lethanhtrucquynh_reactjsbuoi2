import logo from './logo.svg';
import './App.css';
import './index.css'
import RenderModel from './Ex_Thu_Kinh/RenderModel';

function App() {
  return (
    <div className="App">
      <RenderModel/>
    </div>
  );
}

export default App;

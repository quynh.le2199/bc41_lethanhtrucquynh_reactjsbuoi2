import React, { Component } from 'react'
import { dataGlasses } from './data_glasses'

export default class RenderGiaoDien extends Component {
  state = {
    listGlass: dataGlasses,
    img_src: dataGlasses[0].url,
    glassName: dataGlasses[0].name,
    glassPrice: dataGlasses[0].price,
    glassDesc: dataGlasses[0].desc,
  }
  handleChangeGlass = (idGlass) => {
    let id = idGlass;
    this.setState({img_src: dataGlasses[id].url}); 
    this.setState({glassName: dataGlasses[id].name});
    this.setState({glassPrice: dataGlasses[id].price});
    this.setState({glassDesc: dataGlasses[id].desc});
  }
  render() {
    return (
      <div>
        <div className='container model'>
          <div className="row">
            <div className="before col-6"></div>
            <div className="after col-6">
              <img src={this.state.img_src} alt="" />
              <div className="glassInfo">
                <h4>{this.state.glassName}</h4>
                <p className='bg-danger'>${this.state.glassPrice}</p>
                <p>{this.state.glassDesc}</p>
              </div>
            </div>
          </div>
        </div>

        <div className='container glassList'>          
          <div className='border border-dark row py-3' style={{background: 'white'}}>
            {this.state.listGlass.map((item, index) => {
                return (
                  <div onClick={() => {this.handleChangeGlass(item.id-1)}} key={index} className='col-2' style={{backgroundImage: `url(${item.path})`, 
                                                                                                                          backgroundPosition: 'center', 
                                                                                                                          backgroundSize: '80%',
                                                                                                                          backgroundRepeat: 'no-repeat', 
                                                                                                                          height: '90px',
                                                                                                                          cursor: 'pointer'}}></div>
                )
            })}
          </div>

        </div>
      </div>
    )
  }
}
